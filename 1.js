/*1)Щоб створити єлемент потрібно використати метод document.createElement()*/
/*2)Перший параметр визначає, де буде вставлений HTML-код, який передано в функцію. Він може бути:
beforebegin: вставити перед елементом; afterbegin: вставити в початок елементу; beforeend: вставити в кінець елементу; afterend: вставити після елемента*/
/*3)Метод remove() - видаляє елемент зі сторінки, Метод parentNode.removeChild() - видаляє дочірній елемент з батьківського елемента,
element.innerHTML = "" - видалення всіх дочірніх елементів, element.outerHTML = "" - видалення елемента зі сторінки.*/

const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const parent = document.body;

const clearPage = function () {
    const div = document.createElement("div");
    document.body.prepend(div);
    div.style.cssText = `border: 1px solid #ff0000;
        color: red;
        text-align: center;
        width: 50px;
    `;
    let seconds = 3;
    const timer = setInterval(() => {
        div.textContent = seconds;
        seconds--;
        if (seconds < 0) {
            clearInterval(timer);
            document.body.innerHTML = '';
        }
    }, 1000);
}

const list = function (arr,parent){
    const ul = document.createElement("ul");

    const liElements = arr.map((item) => {
        const li = document.createElement("li");
        if (Array.isArray(item)) {
            list(item, li);
        } else {
            li.textContent = item;
        }
        return li;
    });

    liElements.forEach((li) => {
        ul.appendChild(li);
    });

    parent.appendChild(ul);
}

list(array, parent);
clearPage();